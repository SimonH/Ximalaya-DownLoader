package me.ywy.ximalaya;

public class Main {
	public static void main(String[] args) {
		downSound("2872895", false);
	}

	public static void downAlbum(String id, boolean hq) {
		Album album = new Album(id);
		album.init();
		album.down(hq);
	}

	public static void downSound(String id, boolean hq) {
		Sound sound = new Sound(id, null, null);
		if (sound.init()) {
			sound.download(hq);
		}
	}
}
